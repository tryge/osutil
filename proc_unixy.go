// +build !windows,!plan9

package osutil

import (
	"syscall"
)

func isProcessAlivePlatform(pid int) bool {
	err := syscall.Kill(pid, syscall.Signal(0))
	errno := err.(syscall.Errno)
	return errno != syscall.ESRCH
}
