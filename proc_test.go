package osutil

import (
	. "launchpad.net/gocheck"
	"os/exec"
	"syscall"
	"testing"
)

func Test(t *testing.T) { TestingT(t) }

var _ = Suite(&ProcSuite{})

type ProcSuite struct {
}

func (s *ProcSuite) TestSelfAlive(c *C) {
	alive := IsProcessAlive(syscall.Getpid())
	c.Assert(alive, Equals, true)
}

func (s *ProcSuite) TestTerminatedProcessNotAlive(c *C) {
	cmd := exec.Command("echo", "test")
	err := cmd.Run()
	c.Assert(err, IsNil)

	alive := IsProcessAlive(cmd.Process.Pid)
	c.Assert(alive, Equals, false)
}
